package uz.onlineshop.onlineshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.onlineshop.onlineshop.dto.ProductTypeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.security.CurrentUser;
import uz.onlineshop.onlineshop.service.ProductTypeService;

import java.util.UUID;

@RestController
@RequestMapping("/product-type")
@RequiredArgsConstructor
public class ProductTypeController {

    private final ProductTypeService service;

    @PostMapping
    public HttpEntity<?> save(@RequestBody ProductTypeDTO dto, @CurrentUser User user) {
        ResponseData<ProductTypeDTO> result = service.save(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @PutMapping
    public HttpEntity<?> update(@RequestBody ProductTypeDTO dto, @CurrentUser User user) {

        ResponseData result = service.update(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable UUID id) {

        ResponseData result = service.delete(id);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> findById(@PathVariable UUID id) {
        ResponseData result = service.findById(id);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }
}
