package uz.onlineshop.onlineshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.Product;
import uz.onlineshop.onlineshop.service.WarehouseService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/store")
@RequiredArgsConstructor
public class WarehouseStoreController {

    private final WarehouseService service;

    @GetMapping("/{orgId}")
    public HttpEntity<?> findAllByOrgId(@PathVariable("orgId") UUID orgId) {
        ResponseData<List<Product>> result = service.findAllByOrgId(orgId);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }


}
