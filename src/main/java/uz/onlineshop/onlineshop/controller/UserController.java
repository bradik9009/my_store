package uz.onlineshop.onlineshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.onlineshop.onlineshop.dto.ProductTypeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.dto.UserDTO;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.security.CurrentUser;
import uz.onlineshop.onlineshop.service.UserService;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_DIRECTOR')")
    public HttpEntity<?> addUSer(@RequestBody UserDTO dto, @CurrentUser User user) {
        ResponseData<UserDTO> result = service.save(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

}
