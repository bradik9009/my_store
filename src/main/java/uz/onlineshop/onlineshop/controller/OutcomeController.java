package uz.onlineshop.onlineshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.onlineshop.onlineshop.dto.IncomeDTO;
import uz.onlineshop.onlineshop.dto.OutcomeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.security.CurrentUser;
import uz.onlineshop.onlineshop.service.OutcomeService;

import java.time.LocalDate;
import java.util.UUID;

@RestController
@RequestMapping("/outcome")
@RequiredArgsConstructor
public class OutcomeController {

    private final OutcomeService service;

    @PostMapping
    public HttpEntity<?> save(@RequestBody OutcomeDTO dto, @CurrentUser User user) {
        ResponseData<OutcomeDTO> result = service.save(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @PutMapping
    public HttpEntity<?> update(@RequestBody OutcomeDTO dto, @CurrentUser User user) {

        ResponseData result = service.update(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable UUID id) {

        ResponseData result = service.delete(id);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> findById(@PathVariable UUID id) {
        ResponseData result = service.findById(id);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @GetMapping("/get-between-date")
    public HttpEntity<?> getRange(@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                  @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        ResponseData result = service.getRange(from, to);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

}
