package uz.onlineshop.onlineshop.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.onlineshop.onlineshop.dto.LoginDTO;
import uz.onlineshop.onlineshop.service.AuthService;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDto) {
        return authService.authenticateUser(loginDto);
    }

}
