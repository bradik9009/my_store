package uz.onlineshop.onlineshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.onlineshop.onlineshop.dto.OrganizationDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.security.CurrentUser;
import uz.onlineshop.onlineshop.service.OrganizationService;

import java.util.UUID;

@RestController
@RequestMapping("/orgs")
@RequiredArgsConstructor
public class OrganizationController {

    private final OrganizationService organizationService;

    @PostMapping
    public HttpEntity<?> save(@RequestBody OrganizationDTO dto, @CurrentUser User user) {
        ResponseData<OrganizationDTO> result = organizationService.save(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @PutMapping
    public HttpEntity<?> update(@RequestBody OrganizationDTO dto, @CurrentUser User user) {

        ResponseData result = organizationService.update(dto, user);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable UUID id) {

        ResponseData result = organizationService.delete(id);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> findById(@PathVariable UUID id) {
        ResponseData result = organizationService.findById(id);

        if (result.isAccept()) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }

}
