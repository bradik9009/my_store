package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.ProductTypeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;

import java.util.UUID;

public interface ProductTypeService {

    ResponseData<ProductTypeDTO> save(ProductTypeDTO dto, User currentUser);

    ResponseData update(ProductTypeDTO dto, User currentUser);

    ResponseData delete(UUID id);

    ResponseData findById(UUID id);

}
