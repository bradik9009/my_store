package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.dto.UserDTO;
import uz.onlineshop.onlineshop.entity.User;

public interface UserService {

    User findByUsername(String login);

    ResponseData<UserDTO> save(UserDTO dto, User user);
}
