package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.ProductDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;

import java.util.UUID;

public interface ProductService {
    ResponseData<ProductDTO> save(ProductDTO dto, User currentUser);

    ResponseData update(ProductDTO dto, User currentUser);

    ResponseData delete(UUID id);

    ResponseData findById(UUID id);
}
