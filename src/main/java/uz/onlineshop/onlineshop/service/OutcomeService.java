package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.IncomeDTO;
import uz.onlineshop.onlineshop.dto.OutcomeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;

import java.time.LocalDate;
import java.util.UUID;

public interface OutcomeService {

    ResponseData<OutcomeDTO> save(OutcomeDTO dto, User currentUser);

    ResponseData update(OutcomeDTO dto, User currentUser);

    ResponseData delete(UUID id);

    ResponseData findById(UUID id);

    ResponseData getRange(LocalDate from, LocalDate to);
}
