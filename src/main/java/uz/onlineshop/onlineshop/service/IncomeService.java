package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.IncomeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;

import java.time.LocalDate;
import java.util.UUID;

public interface IncomeService {

    ResponseData<IncomeDTO> save(IncomeDTO dto, User user);

    ResponseData update(IncomeDTO dto, User user);

    ResponseData delete(UUID id);

    ResponseData findById(UUID id);

    ResponseData getRange(LocalDate from, LocalDate to);
}
