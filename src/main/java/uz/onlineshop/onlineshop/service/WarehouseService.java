package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.dto.WarehouseDTO;
import uz.onlineshop.onlineshop.entity.Product;
import uz.onlineshop.onlineshop.entity.User;

import java.util.List;
import java.util.UUID;

public interface WarehouseService {

    ResponseData<WarehouseDTO> save(WarehouseDTO dto, User currentUser);

    ResponseData update(WarehouseDTO dto, User currentUser);

    ResponseData delete(UUID id);

    ResponseData findById(UUID id);

    ResponseData<List<Product>> findAllByOrgId(UUID orgId);
}
