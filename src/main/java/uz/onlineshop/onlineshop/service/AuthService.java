package uz.onlineshop.onlineshop.service;

import org.springframework.http.ResponseEntity;
import uz.onlineshop.onlineshop.dto.LoginDTO;

public interface AuthService {
    ResponseEntity<?> authenticateUser(LoginDTO loginDto);
}
