package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.onlineshop.onlineshop.dto.ProductDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.Organization;
import uz.onlineshop.onlineshop.entity.Product;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.repository.ProductRepository;
import uz.onlineshop.onlineshop.service.ProductService;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;

    @Override
    public ResponseData<ProductDTO> save(ProductDTO dto, User currentUser) {
        ResponseData<ProductDTO> result = new ResponseData<>();

        Product product = dto.mapToEntity(currentUser);

        try {
            Product saved = repository.save(product);
            result.setAccept(true);
            result.setMessage("Product successfully saved!");
            result.setData(saved.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error while saving Product data");
        }
        return result;
    }

    @Override
    public ResponseData update(ProductDTO dto, User currentUser) {
        ResponseData result = new ResponseData();

        Product product = repository.findById(dto.getId()).orElse(null);

        if (product == null) {
            result.setAccept(false);
            result.setMessage("Product not found ID = " + dto.getId());
            return result;
        }

        try {
            Product save = repository.save(dto.mapToEntity(currentUser));

            result.setAccept(true);
            result.setMessage("Product successfully updated!");
            result.setData(save.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error saving data");
        }

        return result;
    }

    @Override
    public ResponseData delete(UUID id) {
        ResponseData result = new ResponseData();

        try {
            repository.deleteById(id);
            result.setAccept(true);
            result.setMessage("organization successfully deleted !");
            result.setData(id);

        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error delete data");
        }

        return result;
    }

    @Override
    public ResponseData findById(UUID id) {
        ResponseData result = new ResponseData();

        Product product = repository.findById(id).orElse(null);

        if (product == null) {
            result.setAccept(false);
            result.setMessage("organization not found ID = " + id);
        } else {
            result.setAccept(true);
            result.setData(product.mapToDTO());
        }
        return result;
    }
}
