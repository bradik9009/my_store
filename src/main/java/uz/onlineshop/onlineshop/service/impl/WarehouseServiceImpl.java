package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.dto.WarehouseDTO;
import uz.onlineshop.onlineshop.entity.Product;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.Warehouse;
import uz.onlineshop.onlineshop.repository.WarehouseRepository;
import uz.onlineshop.onlineshop.service.WarehouseService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository repository;

    @Override
    public ResponseData<WarehouseDTO> save(WarehouseDTO dto, User currentUser) {
        ResponseData<WarehouseDTO> result = new ResponseData<>();

        Warehouse warehouse = dto.mapToEntity(currentUser);

        try {
            Warehouse saved = repository.save(warehouse);
            result.setAccept(true);
            result.setMessage("Warehouse successfully saved!");
            result.setData(saved.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error while saving Warehouse data");
        }
        return result;
    }

    @Override
    public ResponseData update(WarehouseDTO dto, User currentUser) {
        ResponseData result = new ResponseData();

        Warehouse warehouse = repository.findById(dto.getId()).orElse(null);

        if (warehouse == null) {
            result.setAccept(false);
            result.setMessage("Warehouse not found ID = " + dto.getId());
            return result;
        }

        try {
            warehouse.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
            warehouse.setUpdatedBy(currentUser.getId());
            Warehouse save = repository.save(dto.mapToEntity(currentUser));

            result.setAccept(true);
            result.setMessage("Warehouse successfully updated!");
            result.setData(save.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error saving data");
        }

        return result;
    }

    @Override
    public ResponseData delete(UUID id) {
        ResponseData result = new ResponseData();

        try {
            repository.deleteById(id);
            result.setAccept(true);
            result.setMessage("organization successfully deleted !");
            result.setData(id);

        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error delete data");
        }

        return result;
    }

    @Override
    public ResponseData findById(UUID id) {
        ResponseData result = new ResponseData();

        Warehouse warehouse = repository.findById(id).orElse(null);

        if (warehouse == null) {
            result.setAccept(false);
            result.setMessage("organization not found ID = " + id);
        } else {
            result.setAccept(true);
            result.setData(warehouse.mapToDTO());
        }
        return result;
    }

    @Override
    public ResponseData<List<Product>> findAllByOrgId(UUID orgId) {
        ResponseData<List<Product>> result = new ResponseData<>();
        List<Product> products = repository.findAllByOrgId(orgId);

        result.setAccept(true);
        result.setData(products);
        return result;
    }
}
