package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.onlineshop.onlineshop.dto.OutcomeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.Income;
import uz.onlineshop.onlineshop.entity.Outcome;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.repository.OutcomeRepository;
import uz.onlineshop.onlineshop.service.OutcomeService;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OutcomeServiceImpl implements OutcomeService {
    
    private final OutcomeRepository repository;

    @Override
    public ResponseData<OutcomeDTO> save(OutcomeDTO dto, User currentUser) {
        ResponseData<OutcomeDTO> result = new ResponseData<>();

        Outcome outcome = dto.mapToEntity(currentUser);

        try {
            Outcome saved = repository.save(outcome);
            result.setAccept(true);
            result.setMessage("outcome successfully saved!");
            result.setData(saved.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error while saving outcome data");
        }
        return result;
    }

    @Override
    public ResponseData update(OutcomeDTO dto, User currentUser) {
        ResponseData result = new ResponseData();

        Outcome outcome = repository.findById(dto.getId()).orElse(null);

        if (outcome == null) {
            result.setAccept(false);
            result.setMessage("outcome not found ID = " + dto.getId());
            return result;
        }

        try {
            Outcome save = repository.save(dto.mapToEntity(currentUser));

            result.setAccept(true);
            result.setMessage("outcome successfully updated!");
            result.setData(save.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error saving data");
        }

        return result;
    }

    @Override
    public ResponseData delete(UUID id) {
        ResponseData result = new ResponseData();

        try {
            repository.deleteById(id);
            result.setAccept(true);
            result.setMessage("outcome successfully deleted !");
            result.setData(id);

        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error delete data");
        }

        return result;
    }

    @Override
    public ResponseData findById(UUID id) {
        ResponseData result = new ResponseData();

        Outcome outcome = repository.findById(id).orElse(null);

        if (outcome == null) {
            result.setAccept(false);
            result.setMessage("outcome not found ID = " + id);
        } else {
            result.setAccept(true);
            result.setData(outcome.mapToDTO());
        }
        return result;
    }

    @Override
    public ResponseData getRange(LocalDate from, LocalDate to) {
        ResponseData result = new ResponseData();

        List<Outcome> allByDateBetween = repository.findAllByDateBetween(from, to);

        result.setAccept(true);
        result.setData(allByDateBetween.stream().map(Outcome::mapToDTO));
        return result;
    }
    
}
