package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import uz.onlineshop.onlineshop.dto.ProductTypeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.Income;
import uz.onlineshop.onlineshop.entity.ProductType;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.repository.ProductTypeRepository;
import uz.onlineshop.onlineshop.service.ProductTypeService;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProductTypeServiceImpl implements ProductTypeService {
    
    private final ProductTypeRepository repository;

    @Override
    public ResponseData<ProductTypeDTO> save(ProductTypeDTO dto, User currentUser) {
        ResponseData<ProductTypeDTO> result = new ResponseData<>();

        ProductType type = dto.mapToEntity(currentUser);

        try {
            ProductType saved = repository.save(type);
            result.setAccept(true);
            result.setMessage("product type successfully saved!");
            result.setData(saved.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error while saving product type data");
        }
        return result;
    }

    @Override
    public ResponseData update(ProductTypeDTO dto, User currentUser) {
        ResponseData result = new ResponseData();

        ProductType type = repository.findById(dto.getId()).orElse(null);

        if (type == null) {
            result.setAccept(false);
            result.setMessage("Income not found ID = " + dto.getId());
            return result;
        }

        try {
            ProductType save = repository.save(dto.mapToEntity(currentUser));

            result.setAccept(true);
            result.setMessage("Income successfully updated!");
            result.setData(save.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error saving data");
        }

        return result;
    }

    @Override
    public ResponseData delete(UUID id) {
        ResponseData result = new ResponseData();

        try {
            repository.deleteById(id);
            result.setAccept(true);
            result.setMessage("Income successfully deleted !");
            result.setData(id);

        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error delete data");
        }

        return result;
    }

    @Override
    public ResponseData findById(UUID id) {
        ResponseData result = new ResponseData();

        ProductType type = repository.findById(id).orElse(null);

        if (type == null) {
            result.setAccept(false);
            result.setMessage("Income not found ID = " + id);
        } else {
            result.setAccept(true);
            result.setData(type.mapToDTO());
        }
        return result;
    }

}
