package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.dto.UserDTO;
import uz.onlineshop.onlineshop.entity.Role;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.enums.RoleName;
import uz.onlineshop.onlineshop.repository.RoleRepository;
import uz.onlineshop.onlineshop.repository.UserRepository;
import uz.onlineshop.onlineshop.service.UserService;
import uz.onlineshop.onlineshop.utils.HelperFunctions;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    @Override
    public ResponseData<UserDTO> save(UserDTO dto, User currentUser) {
        try {
            User user = new User();
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setPhoneNumber(dto.getPhoneNumber());
            user.setUsername(dto.getUsername());
            user.setCreatedBy(currentUser.getId());
            user.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
            if (dto.getPassword() != null) {
                user.setPassword(HelperFunctions.passwordEncode(dto.getPassword()));
            }
            Set<Role> roles = new HashSet<>();
            for (RoleName role : dto.getRoles()) {
                roles.add(roleRepository.findByRoleName(role));
            }
            user.setRoles(roles);
            userRepository.save(user);
            return new ResponseData<>(true, dto, "User successfully added");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseData<>(false, "Could not save user");
        }
    }
}
