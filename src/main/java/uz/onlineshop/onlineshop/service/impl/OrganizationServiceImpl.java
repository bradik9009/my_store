package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import uz.onlineshop.onlineshop.dto.OrganizationDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.Organization;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.repository.OrganizationRepository;
import uz.onlineshop.onlineshop.service.OrganizationService;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository repository;

    @Override
    public ResponseData<OrganizationDTO> save(OrganizationDTO dto, User currentUser) {
        ResponseData<OrganizationDTO> result = new ResponseData<>();

        Organization organization = dto.mapToEntity(currentUser);

        try {
            Organization saved = repository.save(organization);
            result.setAccept(true);
            result.setMessage("Organization successfully saved!");
            result.setData(saved.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error while saving organization data");
        }
        return result;
    }

    @Override
    public ResponseData update(OrganizationDTO dto, User currentUser) {
        ResponseData result = new ResponseData();

        Organization organization = repository.findById(dto.getId()).orElse(null);

        if (organization == null) {
            result.setAccept(false);
            result.setMessage("organization not found ID = " + dto.getId());
            return result;
        }

        try {
            Organization save = repository.save(dto.mapToEntity(currentUser));

            result.setAccept(true);
            result.setMessage("organization successfully updated!");
            result.setData(save.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error saving data");
        }

        return result;
    }

    @Override
    public ResponseData delete(UUID id) {
        ResponseData result = new ResponseData();

        try {
            repository.deleteById(id);
            result.setAccept(true);
            result.setMessage("organization successfully deleted !");
            result.setData(id);

        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error delete data");
        }

        return result;
    }

    @Override
    public ResponseData findById(UUID id) {
        ResponseData result = new ResponseData();

        Organization organization = repository.findById(id).orElse(null);

        if (organization == null) {
            result.setAccept(false);
            result.setMessage("organization not found ID = " + id);
        } else {
            result.setAccept(true);
            result.setData(organization.mapToDTO());
        }
        return result;
    }
}
