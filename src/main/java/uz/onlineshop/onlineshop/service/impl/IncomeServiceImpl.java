package uz.onlineshop.onlineshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.onlineshop.onlineshop.dto.IncomeDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.Income;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.repository.IncomeRepository;
import uz.onlineshop.onlineshop.service.IncomeService;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class IncomeServiceImpl implements IncomeService {

    private final IncomeRepository repository;

    @Override
    public ResponseData<IncomeDTO> save(IncomeDTO dto, User currentUser) {
        ResponseData<IncomeDTO> result = new ResponseData<>();

        Income income = dto.mapToEntity(currentUser);

        try {
            Income saved = repository.save(income);
            result.setAccept(true);
            result.setMessage("Income successfully saved!");
            result.setData(saved.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error while saving Income data");
        }
        return result;
    }

    @Override
    public ResponseData update(IncomeDTO dto, User currentUser) {
        ResponseData result = new ResponseData();

        Income income = repository.findById(dto.getId()).orElse(null);

        if (income == null) {
            result.setAccept(false);
            result.setMessage("Income not found ID = " + dto.getId());
            return result;
        }

        try {
            Income save = repository.save(dto.mapToEntity(currentUser));

            result.setAccept(true);
            result.setMessage("Income successfully updated!");
            result.setData(save.mapToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error saving data");
        }

        return result;
    }

    @Override
    public ResponseData delete(UUID id) {
        ResponseData result = new ResponseData();

        try {
            repository.deleteById(id);
            result.setAccept(true);
            result.setMessage("Income successfully deleted !");
            result.setData(id);

        } catch (Exception e) {
            e.printStackTrace();
            result.setAccept(false);
            result.setMessage("Error delete data");
        }

        return result;
    }

    @Override
    public ResponseData findById(UUID id) {
        ResponseData result = new ResponseData();

        Income income = repository.findById(id).orElse(null);

        if (income == null) {
            result.setAccept(false);
            result.setMessage("Income not found ID = " + id);
        } else {
            result.setAccept(true);
            result.setData(income.mapToDTO());
        }
        return result;
    }

    @Override
    public ResponseData getRange(LocalDate from, LocalDate to) {
        ResponseData result = new ResponseData();

        List<Income> allByDateBetween = repository.findAllByDateBetween(from, to);

        result.setAccept(true);
        result.setData(allByDateBetween.stream().map(Income::mapToDTO));
        return result;
    }
}
