package uz.onlineshop.onlineshop.service;

import uz.onlineshop.onlineshop.dto.OrganizationDTO;
import uz.onlineshop.onlineshop.dto.ResponseData;
import uz.onlineshop.onlineshop.entity.User;

import java.util.UUID;

public interface OrganizationService {
    ResponseData<OrganizationDTO> save(OrganizationDTO dto, User currentUser);

    ResponseData update(OrganizationDTO dto, User currentUser);

    ResponseData delete(UUID id);

    ResponseData findById(UUID id);
}
