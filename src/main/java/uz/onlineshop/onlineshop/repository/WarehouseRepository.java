package uz.onlineshop.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.onlineshop.onlineshop.entity.Product;
import uz.onlineshop.onlineshop.entity.Warehouse;

import java.util.List;
import java.util.UUID;

@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, UUID> {


    @Query(value = "select p from Product p " +
            "left join Warehouse w on w.id = p.warehouse.id " +
            "where w.organization.id = :id")
    List<Product> findAllByOrgId(@Param("id") UUID id);
}
