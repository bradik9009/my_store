package uz.onlineshop.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.onlineshop.onlineshop.entity.Outcome;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface OutcomeRepository extends JpaRepository<Outcome, UUID> {

    List<Outcome> findAllByDateBetween(LocalDate date, LocalDate date2);

}
