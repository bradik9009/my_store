package uz.onlineshop.onlineshop.bootstrap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.onlineshop.onlineshop.entity.Role;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.enums.RoleName;
import uz.onlineshop.onlineshop.repository.RoleRepository;
import uz.onlineshop.onlineshop.repository.UserRepository;

import java.util.Collections;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Value("${spring.sql.init.mode}")
    private String mode;

    @Override
    public void run(String... args) throws Exception {
        if (mode.equals("always")){
            Role director = roleRepository.save(new Role(RoleName.ROLE_DIRECTOR));
            Role manager = roleRepository.save(new Role(RoleName.ROLE_MANAGER));
            Role warehouseManager = roleRepository.save(new Role(RoleName.ROLE_WAREHOUSE_MANAGER));
            userRepository.save(new User(
                    "Director",
                    "Director",
                    "+998993632587",
                    "director",
                    passwordEncoder.encode("director"),
                    Collections.singleton(director)
            ));
        }
    }

}
