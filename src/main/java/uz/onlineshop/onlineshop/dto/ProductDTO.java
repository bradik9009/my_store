package uz.onlineshop.onlineshop.dto;

import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.entity.Product;
import uz.onlineshop.onlineshop.entity.ProductType;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.Warehouse;
import uz.onlineshop.onlineshop.entity.enums.ProductStatus;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class ProductDTO {
    private UUID id;

    private String name;

    private Long incomePrice;

    private Long salePrice;

    private String code;

    private ProductStatus status;

    private UUID typeId;

    private UUID warehouseId;

    public Product mapToEntity(User user) {
        Product product = new Product();

        if (this.id != null) product.setId(this.id);
        if (this.name!= null) product.setName(this.name);
        if (this.incomePrice != null) product.setIncomePrice(this.incomePrice);
        if (this.salePrice != null) product.setSalePrice(this.salePrice);
        if (this.code != null) product.setCode(this.code);
        if (this.status != null) product.setStatus(this.status);
        if (this.typeId != null) product.setType(new ProductType(this.typeId));
        if (this.warehouseId != null) product.setWarehouse(new Warehouse(this.warehouseId));
        product.setCreatedBy(user.getId());
        product.setCreatedAt(Timestamp.from(Instant.now()));
        product.setUpdatedBy(user.getId());
        product.setUpdatedAt(Timestamp.from(Instant.now()));
        return product;
    }
}
