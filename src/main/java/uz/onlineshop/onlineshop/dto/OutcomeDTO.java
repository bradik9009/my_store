package uz.onlineshop.onlineshop.dto;

import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.entity.Outcome;
import uz.onlineshop.onlineshop.entity.ProductType;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.Warehouse;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
public class OutcomeDTO {

    private UUID id;

    private LocalDate date;

    private Long count;

    private UUID productTypeId;

    private UUID warehouseId;

    public Outcome mapToEntity(User user) {
        Outcome outcome = new Outcome();

        if (this.id != null) outcome.setId(this.id);
        if (this.date != null) outcome.setDate(this.date);
        if (this.count != null) outcome.setCount(this.count);
        if (this.productTypeId != null) outcome.setProductType(new ProductType(this.productTypeId));
        if (this.warehouseId != null) outcome.setWarehouse(new Warehouse(this.warehouseId));

        outcome.setCreatedBy(user.getId());
        outcome.setCreatedAt(Timestamp.from(Instant.now()));
        outcome.setUpdatedBy(user.getId());
        outcome.setUpdatedAt(Timestamp.from(Instant.now()));

        return outcome;
    }
    
}
