package uz.onlineshop.onlineshop.dto;

import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.entity.Income;
import uz.onlineshop.onlineshop.entity.ProductType;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.Warehouse;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
public class IncomeDTO {

    private UUID id;

    private LocalDate date;

    private Long count;

    private UUID productTypeId;

    private UUID warehouseId;

    public Income mapToEntity(User user) {
        Income income = new Income();

        if (this.id != null) income.setId(this.id);
        if (this.date != null) income.setDate(this.date);
        if (this.count != null) income.setCount(this.count);
        if (this.productTypeId != null) income.setProductType(new ProductType(this.productTypeId));
        if (this.warehouseId != null) income.setWarehouse(new Warehouse(this.warehouseId));
        income.setCreatedBy(user.getId());
        income.setCreatedAt(Timestamp.from(Instant.now()));
        income.setUpdatedBy(user.getId());
        income.setUpdatedAt(Timestamp.from(Instant.now()));
        return income;
    }

}
