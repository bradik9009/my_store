package uz.onlineshop.onlineshop.dto;

import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.entity.Organization;
import uz.onlineshop.onlineshop.entity.User;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class OrganizationDTO {

    private UUID id;

    private String name;

    public Organization mapToEntity(User user) {
        Organization organization = new Organization();

        if (this.id != null) organization.setId(this.id);

        if (this.name != null) organization.setName(this.name);

        organization.setCreatedBy(user.getId());
        organization.setCreatedAt(Timestamp.from(Instant.now()));
        organization.setUpdatedBy(user.getId());
        organization.setUpdatedAt(Timestamp.from(Instant.now()));
        return organization;
    }
}
