package uz.onlineshop.onlineshop.dto;

import lombok.Data;
import uz.onlineshop.onlineshop.entity.enums.RoleName;

import java.util.Set;
import java.util.UUID;

@Data
public class UserDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String username;
    private String password;
    private Set<RoleName> roles;

}
