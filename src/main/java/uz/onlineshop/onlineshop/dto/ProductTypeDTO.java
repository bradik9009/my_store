package uz.onlineshop.onlineshop.dto;

import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.entity.ProductType;
import uz.onlineshop.onlineshop.entity.User;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class ProductTypeDTO {

    private UUID id;

    private String name;

    public ProductType mapToEntity(User user) {
        ProductType type = new ProductType();
        if (this.id != null) type.setId(this.id);
        if (this.name != null) type.setName(this.name);
        type.setCreatedBy(user.getId());
        type.setCreatedAt(Timestamp.from(Instant.now()));
        type.setUpdatedBy(user.getId());
        type.setUpdatedAt(Timestamp.from(Instant.now()));
        return type;
    }

}
