package uz.onlineshop.onlineshop.dto;

import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.entity.Organization;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.entity.Warehouse;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
public class WarehouseDTO {

    private UUID id;

        private String name;

    private String address;

    private UUID organizationId;

    public Warehouse mapToEntity(User user) {
        Warehouse warehouse = new Warehouse();
        if (this.id != null) warehouse.setId(this.id);
        if (this.name != null) warehouse.setName(this.name);
        if (this.address != null) warehouse.setAddress(this.address);
        if (this.organizationId != null) warehouse.setOrganization(new Organization(this.organizationId));
        warehouse.setCreatedBy(user.getId());
        warehouse.setCreatedAt(Timestamp.from(Instant.now()));
        warehouse.setUpdatedBy(user.getId());
        warehouse.setUpdatedAt(Timestamp.from(Instant.now()));
        return warehouse;
    }
}
