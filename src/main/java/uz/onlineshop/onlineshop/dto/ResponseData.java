package uz.onlineshop.onlineshop.dto;

public class ResponseData<T> {
    private boolean accept;
    private T data;
    private String message;

    public ResponseData() {
    }

    public ResponseData(boolean accept, T data, String message) {
        this.accept = accept;
        this.data = data;
        this.message = message;
    }

    public ResponseData(boolean accept, String message) {
        this.accept = accept;
        this.message = message;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
