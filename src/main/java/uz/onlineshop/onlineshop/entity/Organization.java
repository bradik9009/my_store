package uz.onlineshop.onlineshop.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.onlineshop.onlineshop.dto.OrganizationDTO;
import uz.onlineshop.onlineshop.entity.template.GenericEntity;

import javax.persistence.Entity;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Organization extends GenericEntity {

    private String name;

    public OrganizationDTO mapToDTO() {
        OrganizationDTO dto = new OrganizationDTO();

        dto.setId(super.getId());
        dto.setName(this.name);
        return dto;
    }

    public Organization(UUID id) {
        super.setId(id);
    }

}
