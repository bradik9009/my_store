package uz.onlineshop.onlineshop.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.onlineshop.onlineshop.dto.WarehouseDTO;
import uz.onlineshop.onlineshop.entity.template.GenericEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Warehouse extends GenericEntity {

    private String name;

    private String address;

    @ManyToOne
    private Organization organization;

    public Warehouse(UUID id) {
        super.setId(id);
    }

    public WarehouseDTO mapToDTO() {
        WarehouseDTO dto = new WarehouseDTO();

        dto.setId(super.getId());
        dto.setName(this.name);
        dto.setAddress(this.address);
        dto.setOrganizationId(this.organization.getId());
        return dto;
    }

}
