package uz.onlineshop.onlineshop.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.dto.ProductDTO;
import uz.onlineshop.onlineshop.entity.enums.ProductStatus;
import uz.onlineshop.onlineshop.entity.template.GenericEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class Product extends GenericEntity {

    private String name;

    private Long incomePrice;

    private Long salePrice;

    private String code;

    @Enumerated(EnumType.STRING)
    private ProductStatus status;

    @ManyToOne
    private ProductType type;

    @ManyToOne
    private Warehouse warehouse;

    public ProductDTO mapToDTO() {
        ProductDTO dto = new ProductDTO();

        dto.setId(super.getId());
        dto.setCode(this.code);
        dto.setName(this.name);
        dto.setIncomePrice(this.incomePrice);
        dto.setStatus(this.status);
        dto.setSalePrice(this.salePrice);
        dto.setTypeId(this.type.getId());
        dto.setWarehouseId(this.warehouse.getId());
        return dto;
    }
}
