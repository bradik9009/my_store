package uz.onlineshop.onlineshop.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.dto.IncomeDTO;
import uz.onlineshop.onlineshop.dto.OutcomeDTO;
import uz.onlineshop.onlineshop.entity.template.GenericEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class Outcome extends GenericEntity {

    @ManyToOne
    private ProductType productType;

    private LocalDate date;

    private Long count;

    @ManyToOne
    private Warehouse warehouse;

    public OutcomeDTO mapToDTO() {
        OutcomeDTO dto = new OutcomeDTO();

        dto.setId(super.getId());
        dto.setCount(this.count);
        dto.setDate(this.date);
        dto.setProductTypeId(this.productType.getId());
        dto.setWarehouseId(this.warehouse.getId());
        return dto;
    }
}
