package uz.onlineshop.onlineshop.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.dto.ProductDTO;
import uz.onlineshop.onlineshop.dto.ProductTypeDTO;
import uz.onlineshop.onlineshop.entity.template.GenericEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class ProductType extends GenericEntity {

    @Column(unique = true)
    private String name;

    public ProductType(UUID id) {
        super.setId(id);
    }

    public ProductType() {
    }

    public ProductTypeDTO mapToDTO() {
        ProductTypeDTO dto = new ProductTypeDTO();

        dto.setId(super.getId());
        dto.setName(this.name);
        return dto;
    }
}
