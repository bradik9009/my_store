package uz.onlineshop.onlineshop.entity.enums;

public enum RoleName {
    ROLE_ANONYMOUS,
    ROLE_DIRECTOR,
    ROLE_MANAGER,
    ROLE_WAREHOUSE_MANAGER,
}
