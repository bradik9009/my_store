package uz.onlineshop.onlineshop.entity.enums;

public enum ProductStatus {
    DEFECT,
    ACTIVE,
    SOLD_OUT
}
