package uz.onlineshop.onlineshop.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.onlineshop.onlineshop.dto.IncomeDTO;
import uz.onlineshop.onlineshop.entity.template.GenericEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class Income extends GenericEntity {

    @ManyToOne
    private ProductType productType;

    private LocalDate date;

    private Long count;

    @ManyToOne
    private Warehouse warehouse;

    public IncomeDTO mapToDTO() {
        IncomeDTO dto = new IncomeDTO();

        dto.setId(super.getId());
        dto.setCount(this.count);
        dto.setDate(this.date);
        dto.setProductTypeId(this.productType.getId());
        dto.setWarehouseId(this.warehouse.getId());
        return dto;
    }

}
