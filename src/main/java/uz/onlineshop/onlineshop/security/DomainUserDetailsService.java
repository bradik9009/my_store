package uz.onlineshop.onlineshop.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import uz.onlineshop.onlineshop.entity.User;
import uz.onlineshop.onlineshop.service.UserService;

import javax.transaction.Transactional;

/**
 * Authenticate a user from the database.
 */
@Component(value = "userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {
    //    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);
    private final UserService userService;

    public DomainUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        return loadByLogin(login);
    }

    public User loadByLogin(final String login) {
        User user = userService.findByUsername(login);

        if (user == null)
            throw new UsernameNotFoundException("User " + login + "  not found in database");

        return user;
    }

}
