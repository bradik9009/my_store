${AnsiColor.BRIGHT_RED}                 ,--. ,--.                           ${AnsiColor.CYAN}         ,--.
${AnsiColor.BRIGHT_RED} ,---.  ,--,--,  |  | `--' ,--,--,   ,---.           ${AnsiColor.CYAN}  ,---.  |  ,---.   ,---.   ,---.
${AnsiColor.BRIGHT_RED}| .-. | |      \ |  | ,--. |      \ | .-. :  ,-----. ${AnsiColor.CYAN} (  .-'  |  .-.  | | .-. | | .-. |
${AnsiColor.BRIGHT_RED}' '-' ' |  ||  | |  | |  | |  ||  | \   --.  '-----' ${AnsiColor.CYAN} .-'  `) |  | |  | ' '-' ' | '-' '
${AnsiColor.BRIGHT_RED} `---'  `--''--' `--' `--' `--''--'  `----'          ${AnsiColor.CYAN} `----'  `--' `--'  `---'  |  |-'
${AnsiColor.BRIGHT_RED}                                                     ${AnsiColor.CYAN}                          `--'